import front.OthelloGameBoardView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class OthelloGameApp extends Application {

    private static final String appTitle = "Othello";
    //private static final String appIcon = "file:resources/images/todolist.png";

    private OthelloGameBoardView othelloGameBoardView;

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle(appTitle);
        //primaryStage.getIcons().add(new Image(appIcon));
        othelloGameBoardView = new OthelloGameBoardView(10);
        primaryStage.setScene(new Scene(othelloGameBoardView));
        primaryStage.show();
    }

    public static void main(String[] args){
        launch(args);
    }
}
