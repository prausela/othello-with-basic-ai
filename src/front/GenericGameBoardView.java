package front;

import javafx.geometry.Insets;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
//import sun.rmi.runtime.Log;

public abstract class GenericGameBoardView extends GridPane implements GameBoardView {

    //Sets Board's prefered dimentions
    private static final int PREFERED_WIDTH = 600;
    private static final int PREFERED_HEIGHT = 600;

    protected int rows;
    protected int cols;

    public GenericGameBoardView(int rows, int cols){
        this(rows, cols, PREFERED_WIDTH, PREFERED_HEIGHT);
    }

    public GenericGameBoardView(int rows, int cols, int pref_width, int pref_height){
        this.rows = rows;
        this.cols = cols;
        this.setPrefSize(pref_width, pref_height);
        int i, j;
        for (i = 0; i < rows; i++){
            for (j = 0; j < cols; j++){
                GenericBoardTile tile = tileDefinition();
                if (i % 2 == j % 2)
                    tile.setBackground(new Background(new BackgroundFill(Color.GREEN, CornerRadii.EMPTY, Insets.EMPTY)));
                else
                    tile.setBackground(new Background(new BackgroundFill(Color.DARKGREEN, CornerRadii.EMPTY, Insets.EMPTY)));
                this.add(tile, i, j);
            }
            RowConstraints rowConstraints = new RowConstraints();
            rowConstraints.setPercentHeight(100 / (double) rows);
            this.getRowConstraints().add(rowConstraints);
        }
        for (j = 0; j < cols; j++){
            ColumnConstraints columnConstraints = new ColumnConstraints();
            columnConstraints.setPercentWidth(100 / (double) cols);
            this.getColumnConstraints().add(columnConstraints);
        }
    }

    protected abstract class GenericBoardTile extends StackPane {

        private Text text = new Text();

        GenericBoardTile(){
            //this.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
            text.setFont(Font.font(24));
            this.getChildren().addAll(text);
            this.setOnMouseClicked(event -> {
                if (event.getButton() == MouseButton.PRIMARY){
                    drawX();
                } else if (event.getButton() == MouseButton.SECONDARY) {
                    drawO();
                }
            });
        }

        private void drawX(){
            text.setText("X");
        }

        private void drawO(){
            text.setText("O");
        }
    }
}
